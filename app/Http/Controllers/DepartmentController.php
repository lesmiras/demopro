<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentFormRequest;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::with('employees')->get();

        return view('departments.index', compact('departments'));
    }

    public function create()
    {
        return view('departments.create', [
            'department' => new Department,
        ]);
    }

    public function store(DepartmentFormRequest $request)
    {
        $department = new Department;
        $department->title = $request->title;
        $department->save();

        return response()->redirectToRoute('departments.edit', $department);
    }

    public function edit(Department $department)
    {
        return view('departments.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
        $department->title = $request->title;
        $department->update();

        return response()->redirectToRoute('departments.edit', $department);
    }

    public function destroy(Department $department)
    {
        if ($department->employees->count()) {
            return back()->withError('Department has employees');
        }

        $department->delete();

        return response()->redirectToRoute('departments.index');
    }
}
