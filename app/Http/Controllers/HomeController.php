<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;

class HomeController extends Controller
{
    public function index()
    {
        $departments = Department::all();
        $employees = Employee::with('departments')->get();

        return view('home', compact('employees', 'departments'));
    }
}
