<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeFormRequest;
use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::with('departments')->get();

        return view('employees.index', compact('employees'));
    }

    public function create()
    {
        return view('employees.create', [
            'employee' => new Employee(),
            'departments' => Department::all()
        ]);
    }

    public function store(EmployeeFormRequest $request)
    {
        $employee = new Employee();
        $employee->fill($request->except('departments'));
        $employee->save();
        $employee->departments()->sync($request->departments);

        return response()->redirectToRoute('employees.edit', $employee);
    }

    public function edit(Employee $employee)
    {
        $employee->load('departments');
        $departments = Department::all();

        return view('employees.edit', compact('employee', 'departments'));
    }

    public function update(EmployeeFormRequest $request, Employee $employee)
    {
        $employee->fill($request->all());
        $employee->update();
        $employee->departments()->sync($request->departments);

        return response()->redirectToRoute('employees.edit', $employee);
    }

    public function destroy(Employee $employee)
    {
        $employee->departments()->detach();
        $employee->delete();

        return response()->redirectToRoute('employees.index');
    }
}
