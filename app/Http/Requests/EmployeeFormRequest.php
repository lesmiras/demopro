<?php

namespace App\Http\Requests;

use App\Models\Employee;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeFormRequest extends FormRequest
{
    public function rules()
    {
        $genders = Employee::listGenders();

        return [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'gender' => [
                'required',
                Rule::in(array_keys($genders)),
            ],
            'salary' => 'required|numeric',
            'departments' => 'required|array',
        ];
    }
}
