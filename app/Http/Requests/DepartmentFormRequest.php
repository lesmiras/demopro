<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => 'required|unique:departments|max:255',
        ];
    }
}
