<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['firstname', 'lastname', 'gender', 'salary'];

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'departments_employees');
    }

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function isFemale()
    {
        return $this->gender == self::GENDER_FEMALE;
    }

    public function isMale()
    {
        return $this->gender == self::GENDER_MALE;
    }

    public static function listGenders()
    {
        return [
            self::GENDER_MALE => 'Male',
            self::GENDER_FEMALE => 'Female',
        ];
    }
}