const Vue = require('vue');
const token = document.head.querySelector('meta[name="csrf-token"]');
window.axios = require('axios');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

const app = new Vue({
    el: '#app',
});

$(document).ready(function() {
    $('.select').select2({
        width: '100%'
    });
});