@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Home</h1>
        </div>
        @if ($employees->isNotEmpty())
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    @foreach($departments as $department)
                        <th class="text-center">{{ $department->title }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <th>{{ $employee->fullname }}</th>
                        @foreach($departments as $department)
                            <td class="text-center">{!! $employee->departments->contains($department->id) ? '<i class="fa fa-check"></i>' : '' !!}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-danger" role="alert">
                Empty data!
            </div>
        @endif
    </div>
@endsection