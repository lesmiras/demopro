@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Employees</h1>
            <div class="page-actions float-right">
                <div class="btn-group">
                    <a href="{{ route('employees.create') }}" class="btn btn-primary">create new</a>
                </div>
            </div>
        </div>
        @if ($employees->isNotEmpty())
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fullname</th>
                        <th>Gender</th>
                        <th>Salary</th>
                        <th>Departments</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>{{ $employee->fullname }}</td>
                        <td>
                            <span class="badge badge-{{ $employee->isFemale() ? 'primary' : 'secondary' }}">{{ $employee->gender }}</span>
                        </td>
                        <td>{{ $employee->salary }}</td>
                        <td>
                            @if ($employee->departments->isNotEmpty())
                                {{ $employee->departments->implode('title', ', ') }}
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('employees.edit', $employee) }}" class="btn btn-primary">edit</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-danger" role="alert">
                Empty data!
            </div>
        @endif
    </div>
@endsection