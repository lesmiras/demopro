<div class="form-inner">
    @csrf
    <div class="form-group">
        <label>Firsname:</label>
        <input type="text"
               class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}"
               name="firstname"
               value="{{ old('firstname', $employee->firstname) }}">
        {!! $errors->first('firstname','<small class="invalid-feedback">:message</small>') !!}
    </div>
    <div class="form-group">
        <label>Lastname:</label>
        <input type="text"
               class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}"
               name="lastname"
               value="{{ old('lastname', $employee->lastname) }}">
        {!! $errors->first('lastname','<small class="invalid-feedback">:message</small>') !!}
    </div>
    <div class="form-group">
        <label>Gender:</label>
        <div class="form-check">
            <input type="radio"
                   class="form-check-input {{ $errors->has('gender') ? 'is-invalid' : '' }}"
                   name="gender"
                   id="gender_male"
                   value="male"
                   {{ old('gender', $employee->gender) == 'male' ? 'checked' : '' }}
            >
            <label class="form-check-label" for="gender_male">Male</label>
        </div>
        <div class="form-check">
            <input type="radio"
                   class="form-check-input {{ $errors->has('gender') ? 'is-invalid' : '' }}"
                   name="gender"
                   id="gender_female"
                   value="female"
                    {{ old('gender', $employee->gender) == 'female' ? 'checked' : '' }}
            >
            <label class="form-check-label" for="gender_female">Female</label>
            {!! $errors->first('gender','<small class="invalid-feedback">:message</small>') !!}
        </div>
    </div>
    <div class="form-group">
        <label>Salary:</label>
        <input type="text"
               class="form-control {{ $errors->has('salary') ? 'is-invalid' : '' }}"
               name="salary"
               value="{{ old('salary', $employee->salary) }}">
        {!! $errors->first('salary','<small class="invalid-feedback">:message</small>') !!}
    </div>
    <div class="form-group">
        <label>Departments:</label>
        <select class="custom-select select {{ $errors->has('departments') ? 'is-invalid' : '' }}" name="departments[]" multiple="multiple">
            @foreach($departments as $department)
                <option value="{{ $department->id }}" {{ $employee->departments->contains($department->id) ? 'selected' : '' }}>{{ $department->title }}</option>
            @endforeach
        </select>
        {!! $errors->first('departments','<small class="invalid-feedback">:message</small>') !!}
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</div>