@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Employee - {{ $employee->fullname }} - <small>edit</small></h1>
            <div class="page-actions float-right">
                <form action="{{ route('employees.destroy', $employee) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="btn-group">
                        <button type="submit" class="btn btn-danger">delete</button>
                    </div>
                </form>
            </div>
        </div>
        <form action="{{ route('employees.update', $employee) }}" enctype="multipart/form-data" method="post">
            {{ method_field('PUT') }}
            @include('employees._form')
        </form>
    </div>
@endsection