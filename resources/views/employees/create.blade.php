@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Employees - New</h1>
        </div>
        <form action="{{ route('employees.store') }}" enctype="multipart/form-data" method="post">
            @include('employees._form')
        </form>
    </div>
@endsection