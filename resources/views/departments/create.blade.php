@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Departments - New</h1>
        </div>
        <form action="{{ route('departments.store') }}" enctype="multipart/form-data" method="post">
            @include('departments._form')
        </form>
    </div>
@endsection