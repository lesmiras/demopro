@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Departments - {{ $department->title }} - <small>edit</small></h1>
            <div class="page-actions float-right">
                <form action="{{ route('departments.destroy', $department) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="btn-group">
                        <button type="submit" class="btn btn-danger">delete</button>
                    </div>
                </form>
            </div>
        </div>
        @if (\Session::has('error'))
            <div class="alert alert-danger">
                {!! \Session::get('error') !!}
            </div>
        @endif
        <form action="{{ route('departments.update', $department) }}" enctype="multipart/form-data" method="post">
            {{ method_field('PUT') }}
            @include('departments._form')
        </form>
    </div>
@endsection