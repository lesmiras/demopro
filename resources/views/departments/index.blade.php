@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header clearfix">
            <h1 class="page-title float-left">Departments</h1>
            <div class="page-actions float-right">
                <div class="btn-group">
                    <a href="{{ route('departments.create') }}" class="btn btn-primary">create new</a>
                </div>
            </div>
        </div>
        @if ($departments->isNotEmpty())
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Employees</th>
                        <th>Max. salary</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($departments as $department)
                    <tr>
                        <th>1</th>
                        <td>{{ $department->title }}</td>
                        <td>{{ $department->employees->count() }}</td>
                        <td>{{ $department->employees->max('salary') }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('departments.edit', $department) }}" class="btn btn-primary">edit</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-danger" role="alert">
                Empty data!
            </div>
        @endif
    </div>
@endsection