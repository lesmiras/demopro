<div class="form-inner">
    @csrf
    <div class="form-group">
        <label>Title</label>
        <input type="text"
               class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
               name="title"
               value="{{ old('title', $department->title) }}">
        {!! $errors->first('title','<small class="invalid-feedback">:message</small>') !!}
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</div>