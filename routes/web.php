<?php

Route::get('/', 'HomeController@index')->name('home.index');
Route::resource('departments', 'DepartmentController')->except(['show']);
Route::resource('employees', 'EmployeeController')->except(['show']);
